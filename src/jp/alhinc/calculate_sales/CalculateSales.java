package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "ファイルのフォーマットが不正です";
	private static final String SALE_FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String INVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String INVALID_COMMODITY_CODE = "の商品コードが不正です";
	private static final String UPPER_LIMIT_OVER = "合計金額が10桁を超えました";

	// エラー時の対象ファイル
	private static final String BRANCH_FILE = "支店定義";
	private static final String COMMODITY_FILE = "商品定義";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();


		// コマンドライン引数が渡されているか確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		// 3桁の数字, エラー時に表するファイル種別
		String regex = "^[0-9]{3}$";
		String fileType = BRANCH_FILE;

		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, regex, fileType)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		// 8桁の英数字, エラー時に表するファイル種別
		regex = "^[a-zA-Z0-9]{8}$";
		fileType = COMMODITY_FILE;

		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, regex, fileType)) {
			return;
		}


		// ※ここから集計処理を作成してください。(処理内容2-1)
		// PATH先フォルダ内にあるファイルを取得
		File[] files = new File(args[0]).listFiles();

		// 売上げファイルのみを追加する用のList
		List<File> rcdFiles = new ArrayList<>();

		// 売上げファイルか判別
		for(int i = 0; i < files.length; i++) {
			//ファイルであり、かつファイル名が8桁の数字.rcdであるか判別
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}\\.rcd$")) {
				//^[0-9]{8}.rcd$に一致するものを追加
				rcdFiles.add(files[i]);
			}
		}

		// 売上ファイルの連番確認
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(NOT_SERIAL_NUMBER);
				return;
			}
		}

		// ※処理内容2-2
		BufferedReader br = null;

		// 売上げファイルの数だけ処理を行う
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				// 売上げファイルのパスを渡し、中身を読み取る
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				// 売上ファイル内のデータを入れるList
				List<String> saleFileData = new ArrayList<>();
				// 読み取った行を入れるString
				String line = null;

				// 売上ファイルのデータをListへ追加
				while((line = br.readLine()) != null) {
					saleFileData.add(line);
				}

				// 売上ファイルのフォーマットを確認
				if(saleFileData.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALE_FILE_INVALID_FORMAT);
					return;
				}
				
				// 各コードを格納
				String branchCode = saleFileData.get(0);
				String commodityCode = saleFileData.get(1);

				// 売上ファイルの支店コードを確認
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_BRANCH_CODE);
					return;
				}

				// 売上ファイルの商品コードを確認
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_COMMODITY_CODE);
					return;
				}

				// 売上金額が数字か確認
				if(!saleFileData.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// 売上金額をString>longへ変換
				long fileSales = Long.parseLong(saleFileData.get(2));
				// 売上金額を合算, b=branch, c=commodity
				Long branchSaleAmount = branchSales.get(branchCode) + fileSales;
				Long commoditySaleAmount = commoditySales.get(commodityCode) + fileSales;

				// 支店・商品両方の売上金額最大10桁を確認
				if(branchSaleAmount >= 1000000000L || commoditySaleAmount >= 1000000000L) {
					System.out.println(UPPER_LIMIT_OVER);
					return;
				}

				// 売上金額合計をMapへ格納
				branchSales.put(branchCode, branchSaleAmount);
				commoditySales.put(commodityCode, commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();

					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param コードと名前を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @param ファイルフォーマットを判定する用の正規表現
	 * @param エラー用ファイル名
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> itemNames, Map<String, Long> itemSales, String regex, String fileType) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// 支店定義ファイルが存在するかを判定
			if(!file.exists()) {
				System.out.println(fileType + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)

				// コード, コードに対応する名前 を分割してString[] itemsへ代入する
				// items[0]:コード
				// items[1]:コードに対応する名前
				String[] items = line.split(",");
				

				// コードと名前が","で区切られていて, かつコードがregexで定義されたフォーマットであること
				if((items.length != 2) || !(items[0].matches(regex))) {
					System.out.println(fileType + FILE_INVALID_FORMAT);
					return false;
				}

				// 支店コード, 支店名をbranchNames, branchSalesへ代入する
				itemNames.put(items[0], items[1]);
				itemSales.put(items[0], 0L);
				//L Longへキャスト
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param コードと名前を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> itemNames, Map<String, Long> itemSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			// ファイル書込先準備
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			// 書込内容を取得
			// 支店名Mapからキーを取得
			for(String key : itemNames.keySet()) {
				// キー（コード）,コードに対応する名前, 総売上げ を書き込む
				bw.write(key + "," + itemNames.get(key) + "," + itemSales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			try {
				// ファイルを開いている場合
				if(bw != null) {
					// bwを閉じる
					bw.close();
				}
			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;
			}
		}
		return true;
	}

}
